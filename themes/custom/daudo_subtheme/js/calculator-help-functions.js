function getCurrentDate() {
  const now = new Date();
  const year = now.getFullYear();
  const month = String(now.getMonth() + 1).padStart(2, '0');
  const day = String(now.getDate()).padStart(2, '0');
  return `${year}-${month}-${day}`;
}

function getDataFromCookie() {
  var name = 'currency_data=';
  var decodedCookie = decodeURIComponent(document.cookie);
  var cookieArray = decodedCookie.split(';');

  for (var i = 0; i < cookieArray.length; i++) {
    var cookie = cookieArray[i];

    while (cookie.charAt(0) === ' ') {
      cookie = cookie.substring(1);
    }

    if (cookie.indexOf(name) === 0) {
      return JSON.parse(cookie.substring(name.length, cookie.length));
    }
  }

  return null; // Return null if the cookie is not found
}

function fetchFile(fileUrl) {
  return fetch(fileUrl)
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.arrayBuffer();
    });
}

function processFileData(data) {
  const workbook = XLSX.read(data, { type: 'array' });
  const sheetName = workbook.SheetNames[0];
  const sheet = workbook.Sheets[sheetName];
  return XLSX.utils.sheet_to_json(sheet);
}

function getEuroRate(value, $ATeuroCourse) {
  if (value && value > 0) {
    return value;
  }
  else {
    return $ATeuroCourse.val();
  }
}

function calcCopartFees() {
  AT.winPriceValue = parseInt($ATwinPrice.val());

  let result;

  for (let key in AT.copartPricesArray) {
    if (AT.copartPricesArray.hasOwnProperty(key)) {
      const data = AT.copartPricesArray[key];
      const bidPriceRange = data["Final Bid Price"];
      const [lowerStr, upperStr] = bidPriceRange.split('-');
      const lower = parseFloat(lowerStr.replace(/\$|,/g, ''));
      const upper = parseFloat(upperStr.replace(/\$|,/g, ''));

      if (AT.winPriceValue >= lower && AT.winPriceValue <= upper) {
        result = data["Non-Clean Title"];
      }
    }
  }

  if (result.includes('%')) {
    result = parseFloat(result.replace('%', ''));
    AT.copartPrice = AT.winPriceValue * result / 100;
  }
  else {
    AT.copartPrice = parseFloat(result.replace('$', ''));
  }
}

function calcCopartBidFees() {
  AT.winPriceValue = parseInt($ATwinPrice.val());

  let result;

  for (let key in AT.copartBidFeesArray) {
    if (AT.copartBidFeesArray.hasOwnProperty(key)) {
      const data = AT.copartBidFeesArray[key];
      const bidPriceRange = data["Bid"];
      const [lowerStr, upperStr] = bidPriceRange.split('-');
      const lower = parseFloat(lowerStr.replace(/\$|,/g, ''));
      const upper = parseFloat(upperStr.replace(/\$|,/g, ''));

      if (AT.winPriceValue >= lower && AT.winPriceValue <= upper) {
        result = data["Value"];
      }
    }
  }

  AT.copartBidFees = parseFloat(result.replace('$', ''));
}

function calcCopartOtherFees() {
  let result = 0;

  for (let key in AT.copartFeesArray) {
    if (AT.copartFeesArray.hasOwnProperty(key)) {
      const data = AT.copartFeesArray[key];
      result = result + parseFloat(data["Value"].replace('$', ''))
    }
  }

  AT.copartOtherFees = result;
}

function calcIAAIFees() {
  AT.winPriceValue = parseInt($ATwinPrice.val());

  let result;

  for (let key in AT.iaaiPricesArray) {
    if (AT.iaaiPricesArray.hasOwnProperty(key)) {
      const data = AT.iaaiPricesArray[key];
      const bidPriceRange = data["Price"];
      const [lowerStr, upperStr] = bidPriceRange.split('-');
      const lower = parseFloat(lowerStr.replace(/\$|,/g, ''));
      const upper = parseFloat(upperStr.replace(/\$|,/g, ''));

      if (AT.winPriceValue >= lower && AT.winPriceValue <= upper) {
        result = data["Fee"];
      }
    }
  }

  if (result.includes('%')) {
    result = parseFloat(result.replace('%', ''));
    AT.iaaiPrice = AT.winPriceValue * result / 100;
  }
  else {
    AT.iaaiPrice = parseFloat(result.replace('$', ''));
  }
}

function calcIAAIBidFees() {
  AT.winPriceValue = parseInt($ATwinPrice.val());

  let result;

  for (let key in AT.iaaiBidFeesArray) {
    if (AT.iaaiBidFeesArray.hasOwnProperty(key)) {
      const data = AT.iaaiBidFeesArray[key];
      const bidPriceRange = data["Price"];
      const [lowerStr, upperStr] = bidPriceRange.split('-');
      const lower = parseFloat(lowerStr.replace(/\$|,/g, ''));
      const upper = parseFloat(upperStr.replace(/\$|,/g, ''));

      if (AT.winPriceValue >= lower && AT.winPriceValue <= upper) {
        result = data["Live Online Bid Fee"];
      }
    }
  }

  AT.iaaiBidFees = parseFloat(result.replace('$', ''));
}

function calcIAAIOtherFees() {
  let result = 0;

  for (let key in AT.iaaiFeesArray) {
    if (AT.iaaiFeesArray.hasOwnProperty(key)) {
      const data = AT.iaaiFeesArray[key];
      result = result + parseFloat(data["Value"].replace('$', ''))
    }
  }

  AT.iaaiOtherFees = result;
}

/**
 * Update $port <select> with options based by selected Auction name
 * @param Auction
 */
function updatePortOptions(Auction, Promo, CurrentPort) {
  let filteredData;
  // Filter objects based on the 'Auction' value
  if (Promo) {
    filteredData = AT.cargoloopOptLandArray.filter(obj => obj.Auction === Auction);
  }
  else {
    filteredData = AT.cargoloopLandArray.filter(obj => obj.Auction === Auction);
  }

  filteredData.sort((a, b) => {
    // First, compare by State
    const stateComparison = a.State.localeCompare(b.State);
    // If States are the same, then compare by City
    if (stateComparison === 0) {
      return a.City.localeCompare(b.City);
    }

    return stateComparison;
  });


  $ATport.empty();

  let $options = [];

  $options.push(jQuery('<option>', {
    value: '',
    text: '-Немає-',
  }));

  // Create and add new options based on the filtered data
  filteredData.forEach(obj => {
    const optionText = `${obj.State} - ${obj.City}`;
    const optionValue = `${obj.State} - ${obj.City}`;
    const $option = jQuery('<option>', {
      value: optionValue,
      text: optionText,
      data: obj // Set the entire object as the 'data' attribute
    });

    $options.push($option);
  });

  $ATport.append($options);

  if (CurrentPort) {
    $ATport.val(CurrentPort);
    const selectedData = $ATport.find('option:selected').data();
    AT.landTargetCode = selectedData.Target;
    findLandTransfer(selectedData, AT.landTargetCode);
  }
}

/**
 * Finds Land Transfer Value by Target
 * Auction Location	  Auction	  City	     State	Zip	    Savannah,GA	  Elizabeth,NJ	  Houston,TX	  Los Angeles,CA	  Indianapolis,IN	  Target
  ANCHORAGE	          Copart	  ANCHORA	   AK	    99501    3450	        3050	          2850	        ->2650	            2650	          ->CA
 */
function findLandTransfer(selectedData, Target) {
  const keys = Object.keys(selectedData).slice(6, 11);
  // Search for the key ending with the 'Target' value
  const matchingKey = keys.find(key => key.endsWith(Target));
  // Update Land Transfer Label.
  AT.transferLabelUpdated = ' (' + matchingKey + ')';
  // Update Sea Transfer.
  AT.seaTransferLabelUpdated = ' з порта ' + matchingKey;
  // Update Sea Transfer Label.
  $ATtransferSeaLabel.val(AT.seaTransferLabelUpdated);

  const targetStateArr = matchingKey.split(',');
  AT.landTargetName = targetStateArr[0];
  // Update Sea Transfer Value.
  checkPromoCode();
  findSeaTransfer(AT.promo);
  $ATcalculatedTransferSea.val(AT.seaTransfer);

  // Return the corresponding value if found, otherwise return null
  AT.landTransfer = matchingKey ? parseInt(selectedData[matchingKey]) : 0;
  // Update Transfer Label.
  $ATtransferLabel.text(AT.transferLabelOriginal + AT.transferLabelUpdated);
  // Update Land Transfer Value.
  $ATcalculatedTransferPrice.val(AT.landTransfer);

  return AT.landTransfer;
}

function findSeaTransfer(Promo) {
  if (Promo) {
    for (const item of AT.cargoloopOptSeaArray) {
      if (item.Port === AT.landTargetName) {
        AT.seaTransfer = item.Price;
      }
    }
  }
  else {
    for (const item of AT.cargoloopSeaArray) {
      if (item.Port === AT.landTargetName) {
        AT.seaTransfer = item.Price;
      }
    }
  }
}

function checkPromoCode() {
  let promoValArray = $ATpromo.val();
  promoValArray = promoValArray.split('|');
  let userPromo = $ATUserPromo.val();
  AT.promo = promoValArray.includes(userPromo);

  return AT.promo;
}

function updateAllVariables(allFilesData) {
  // 0. Get Promo
  checkPromoCode();
  // 1. Get Euro Rate.
  AT.euroRateValue = getEuroRate(allFilesData[allFilesData.length - 1], $ATeuroCourse);
  // 2. Get Auction Name.
  AT.auctionName = $ATauction.val();

  // 3. Get Cargoloop Land Prices
  AT.cargoloopLandArray = allFilesData[0];
  // Get Cargoloop Sea Prices
  AT.cargoloopSeaArray = allFilesData[1];
  // 3. Get Cargoloop Opt Land Prices
  AT.cargoloopOptLandArray = allFilesData[2];
  // Get Cargoloop Opt Sea Prices
  AT.cargoloopOptSeaArray = allFilesData[3];

  // Get Copart Prices
  AT.copartPricesArray = allFilesData[4];
  // Get Copart Bid Fees
  AT.copartBidFeesArray = allFilesData[5];
  // Get Copart Additional Fees
  AT.copartFeesArray = allFilesData[6];
  // Calc Copart Other Fees (do not related to the price, just static values
  calcCopartOtherFees();

  // Get IAAI Prices
  AT.iaaiPricesArray = allFilesData[7];
  // Get IAAI Bid Fees
  AT.iaaiBidFeesArray = allFilesData[8];
  // Get IAAI Additional Fees
  AT.iaaiFeesArray = allFilesData[9];
  // Calc IAAI Other Fees (do not related to the price, just static values
  calcIAAIOtherFees();

  AT.transfer2Ukraine = parseInt($ATtransfer2Ukraine.val());

  // 4. Get Win Price.
  AT.winPriceValue = parseInt($ATwinPrice.val()); // Финальная ставка значение инпута

  AT.customAdditionalPrice = parseInt($ATform.find('input[data-drupal-selector="edit-custom-additional-price"]').val());

  // AT.promo = $form.find('input[data-drupal-selector="edit-promo"]').val(); // Later
}

function updateAllFields() {
  // 2. Update Form's value.
  $ATeuroCourse.val(AT.euroRateValue);
  // Update Transfer Label.
  $ATtransferLabel.text(AT.transferLabelOriginal + AT.transferLabelUpdated);
  // Update Land Transfer Value.
  $ATcalculatedTransferPrice.val(AT.landTransfer);
  // Update Sea Transfer Label.
  $ATtransferSeaLabel.val(AT.seaTransferLabelUpdated);
  // Update Sea Transfer Value.
  $ATcalculatedTransferSea.val(AT.seaTransfer);
  // Update Auction Fees

  if (AT.auctionName === 'Copart') {
    const copartTotal = AT.copartPrice + AT.copartBidFees + AT.copartOtherFees;
    $ATauctionValue.val('$' + copartTotal);
  }

  if (AT.auctionName === 'IAAI') {
    const iaaiTotal = AT.iaaiPrice + AT.iaaiBidFees + AT.iaaiOtherFees;
    $ATauctionValue.val('$' + iaaiTotal);
  }

  $ATcalculatedPrice.val(AT.winPriceValue);

  $ATcustoms.val(AT.calculatedAcciz + AT.calculatedPoshlina + AT.calculatedNds);
  $ATposhlina.val(AT.calculatedPoshlina);
  $ATacciz.val(AT.calculatedAcciz);
  $ATnds.val(AT.calculatedNds);
  $ATfund.val(AT.calculatedPension);

  if (AT.promo) {
    AT.calculatedServices = 300;

  }
  else {
    AT.calculatedServices = 500;
  }

  $ATservices.val(AT.calculatedServices);

  $ATsumInOdessa.val(
    AT.winPriceValue +
    AT.calculatedAuctionValue +
    AT.landTransfer + AT.seaTransfer +
    AT.calculatedExpedition +
    AT.calculatedAcciz +
    AT.calculatedPoshlina +
    AT.calculatedNds +
    AT.calculatedServices +
    AT.transfer2Ukraine +
    parseInt($ATdocs_ua.val()) + parseInt($ATdocs_usa.val())
  );
}

function getCarAge(makeYearValue) {
  var currentYear = parseInt(new Date().getFullYear());

  if (makeYearValue && makeYearValue !== 'old') {
    var yearDiff = currentYear - parseInt(makeYearValue);

    if (yearDiff < 3) {
      return 1;
    }
    else {
      return yearDiff - 1;
    }
  }
  else {
    return 15;
  }
}

function calcAczizBens(capacity, koef, year) {
  var tarif,
    akciz;

  tarif = (capacity <= AT.accizLimits[1]['limit_bens']) ? AT.accizLimits[1]['limit_bens_price'][0]: AT.accizLimits[1]['limit_bens_price'][1];
  akciz =  tarif * capacity * year;

  return Math.round(akciz * koef);
}

function calcAczizDizel(capacity, koef, year) {
  var tarif,
    akciz;

  tarif = (capacity <= AT.accizLimits[1]['limit_dizel']) ? AT.accizLimits[1]['limit_dizel_price'][0] : AT.accizLimits[1]['limit_dizel_price'][1];
  akciz = tarif * capacity * year;

  return Math.round(akciz * koef);
}

function calculatePoshlina(price) {
  return Math.round(price * 0.1);
}

function calculateNds(price, poshlina, acciz) {
  return Math.round((price + poshlina + acciz) * 0.2);
}

function calculatePension(price) {
  return Math.round(price * 0.03);
}

function makeCalc() {
  checkPromoCode();
  AT.auctionName = $ATauction.val();
  const currentPort = $ATport.val();

  updatePortOptions(AT.auctionName, AT.promo, currentPort);

  if (AT.auctionName === 'Copart') {
    calcCopartFees();
    calcCopartBidFees();
    const copartTotal = AT.copartPrice + AT.copartBidFees + AT.copartOtherFees;
    AT.calculatedAuctionValue = copartTotal;
    $ATauctionValue.val('$' + copartTotal);
  }

  if (AT.auctionName === 'IAAI') {
    calcIAAIFees();
    calcIAAIBidFees();
    const iaaiTotal = AT.iaaiPrice + AT.iaaiBidFees + AT.iaaiOtherFees;
    AT.calculatedAuctionValue = iaaiTotal;
    $ATauctionValue.val('$' + iaaiTotal);
  }

  var engineVolumeValue = $ATengineVolume.val(),
    vehicleType = $ATform.find('.form-item-vehicle-type input[type=radio]:checked').val(),
    fuelType = $ATform.find('.form-item-fuel input[type=radio]:checked').val(),
    batteryValue = $ATbattery.val(),
    makeYearValue = $ATmakeYear.val(),
    makeYearKoeff = getCarAge(makeYearValue);

  switch(fuelType){
    case "Бензин":
      AT.calculatedAcciz = calcAczizBens(engineVolumeValue, AT.euroRateValue, makeYearKoeff);
      AT.calculatedPoshlina = calculatePoshlina(AT.winPriceValue + AT.customAdditionalPrice + AT.calculatedAuctionValue);
      AT.calculatedNds = calculateNds(AT.winPriceValue + AT.customAdditionalPrice + AT.calculatedAuctionValue, AT.calculatedAcciz, AT.calculatedPoshlina);
      break;

    case "Дизель":
      AT.calculatedAcciz = calcAczizDizel(engineVolumeValue, AT.euroRateValue, makeYearKoeff);
      AT.calculatedPoshlina = calculatePoshlina(AT.winPriceValue + AT.customAdditionalPrice + AT.calculatedAuctionValue);
      AT.calculatedNds = calculateNds(AT.winPriceValue + AT.customAdditionalPrice + AT.calculatedAuctionValue, AT.calculatedAcciz, AT.calculatedPoshlina);
      break;

    case "Электро":
      AT.calculatedAcciz = Math.round(batteryValue * AT.euroRateValue);
      AT.calculatedPoshlina = 0;
      AT.calculatedNds = 0;
      break;

    case "Гибрид":
      AT.calculatedAcciz = Math.round(100 * AT.euroRateValue);
      AT.calculatedPoshlina = calculatePoshlina(AT.winPriceValue + AT.customAdditionalPrice + AT.calculatedAuctionValue);
      AT.calculatedNds = calculateNds(AT.winPriceValue + AT.customAdditionalPrice + AT.calculatedAuctionValue, AT.calculatedAcciz, AT.calculatedPoshlina);
      break;
  }

  AT.calculatedPension = calculatePension(AT.winPriceValue + AT.customAdditionalPrice + AT.calculatedAuctionValue);
  AT.calculatedExpedition = parseInt($ATexpedition.val());

  if (AT.promo) {
    AT.calculatedServices = 300;

  }
  else {
    AT.calculatedServices = 500;
  }

  updateAllFields();
}
