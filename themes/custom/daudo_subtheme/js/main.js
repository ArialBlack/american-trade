(function ($) {
  Drupal.behaviors.atCommon = {
    attach: function (context, settings) {

      $(once('atCommon', 'body', context)).each(function () {
        var $servicesTextRotatorTrigger = $('#services-text-rotator .link a'),
          $servicesModal = $('#contentBottomWebform'),
          $stepsTrigger = $('.service-timeline li:first-child a.btn'),
          $stepsModal = $('#requestCarWebform');

        $servicesTextRotatorTrigger.on('click', function(e) {
          e.preventDefault();
          $servicesModal.modal();
        });

        $stepsTrigger.on('click', function(e) {
          e.preventDefault();
          $stepsModal.modal();
        });
      });

      $(once('atTrack', '#track-form', context)).each(function () {
        var $form = $(this),
          vin = $form.find('input'),
          $submitButton = $form.find('button'),
          $trackNotice = $('#track-notice-ae'),
          $iframeAE = $('#track-info-ae'),
          $iframeCargo = $('#track-info-cargo');

        $submitButton.on('click', function(e) {
          e.preventDefault();
          var vinCode = vin.val();

          if(/^[A-HJ-NPR-Za-hj-npr-z\d]{8}[\dX][A-HJ-NPR-Za-hj-npr-z\d]{2}\d{6}$/.test(vinCode)) {
            $iframeAE.attr('src', 'https://client.atlanticexpresscorp.com/order/' + vinCode + '/track').show();

            $.ajax({
              url: 'https://american-trade-cargoloop-078d8c01b4ed.herokuapp.com/search?q=' + vinCode,
              type: 'GET',
              success: function(data) {
                // Do something with the data returned from the endpoint
                let cargoloopIframe = 'https://www.cargoloop.com/Unit/View.aspx?id=' + data;
                $iframeCargo.attr('src', cargoloopIframe).show();
              },
              error: function(xhr, status, error) {
              }
            });

            $trackNotice.hide();
          } else {
            $iframeAE.attr('src', '').hide();
            $iframeCargo.attr('src', '').hide();
            $trackNotice.show();
          }
        });
      });
    }
  };
}(jQuery));
