(function ($, Drupal) {
  Drupal.behaviors.atCalculator = {
    attach: function (context, settings) {

      $(once('atCalculator', 'form.webform-submission-transfer-calculator-form', context)).each(function () {
        const $loadingScreen = $('#loading-screen');
        $loadingScreen.show();

        function getDataFromTables() {
          const fileUrls = [
            '/cargoloop-land-prices.xlsx',
            '/cargoloop-sea-prices.xlsx',
            '/cargoloop-land-prices-opt.xlsx',
            '/cargoloop-sea-prices-opt.xlsx',

            '/copart-prices.xlsx',
            '/copart-bid-fees.xlsx',
            '/copart-fees.xlsx',

            '/iaai-prices.xlsx',
            '/iaai-bid-fees.xlsx',
            '/iaai-fees.xlsx',
          ];

          // Array to store promises for each file fetch
          const promises = [];

          fileUrls.forEach(fileUrl => {
            const promise = fetchFile(fileUrl)
              .then(data => processFileData(data))
              .catch(error => console.error('Error fetching or processing file:', error));

            promises.push(promise);
          });

          const getEuroRateAPI = new Promise((resolve, reject) => {
            const api = 'https://api.freecurrencyapi.com/v1/latest?apikey=fca_live_AqUVQALKB5dLzqaTvycZlN6798gpMv1tE9x4LP0V&currencies=USD&base_currency=EUR';

            $.get(api)
              .done(function(data) {
                resolve(data.data.USD);
                // Store the API response in a cookie with a one-day lifetime
                let currentDate = getCurrentDate();
                let expires = new Date(new Date().getTime() + 24 * 60 * 60 * 1000).toUTCString();
                document.cookie = `currency_data=${JSON.stringify(data.data.USD)}; expires=${expires}; path=/`;
                resolve(parseFloat(data.data.USD)); // Resolve the AJAX request promise with the received data
              })
              .fail(function() {
                reject(new Error('Помилка отримання курсу'));
              });
          });

          promises.push(getEuroRateAPI);

          // Wait for all promises to resolve
          Promise.all(promises)
            .then(allFilesData => {
              // Here, allFilesData will be an array containing the JSON data of each processed file
              console.log('All files fetched and processed:');
              $loadingScreen.hide();

              //$('.trade-calculator--disabled').removeClass('trade-calculator--disabled');
              updateAllVariables(allFilesData);
              updateAllFields();

              $('#get-auctions').show();

            })
            .catch(error => console.error('Error fetching or processing files:', error));
        }

        // 1. Define jQuery elements.
        $ATform = $(this);
        $ATsubmitButton = $ATform.find('.calc-button');
        $ATcustomsInfoTrigger = $ATform.find('.form-item-customs');
        $ATwinPrice = $ATform.find('#edit-price');
        $ATengineVolume = $ATform.find('#edit-volume');
        $ATbattery = $ATform.find('#edit-batareya-kvt-ch');
        $ATmakeYear = $ATform.find('#edit-age');
        $ATcalculatedPrice = $ATform.find('#edit-price-value');
        $ATauction = $ATform.find('#edit-auction');
        $ATauctionValue = $ATform.find('#edit-auction-value');
        $ATcalculatedTransferPrice = $ATform.find('#edit-city-value');
        $ATcalculatedTransferSea = $ATform.find('.form-item-sea-transfer input');
        $ATexpedition = $ATform.find('#edit-expedition');
        $ATservices = $ATform.find('#edit-services');
        $ATport = $ATform.find('#edit-city');
        $ATtransferLabel = $ATform.find('.form-item-city label');
        $ATtransferSeaLabel = $ATform.find('.form-item-sea-transfer label');
        $ATeuroCourse = $ATform.find('#edit-euro');
        $ATcustoms = $ATform.find('#edit-customs');
        $ATposhlina = $ATform.find('#edit-poshlina');
        $ATacciz = $ATform.find('#edit-akciznyy-sbor');
        $ATnds = $ATform.find('#edit-nalog-na-dobavlennuyu-stoimost-nds-');
        $ATfund = $ATform.find('#edit-fund');
        $ATsumInOdessa = $ATform.find('#edit-sum');
        $ATdocs_ua = $ATform.find('#edit-docs-ua');
        $ATdocs_usa = $ATform.find('#edit-docs-usa');
        $ATpromo = $ATform.find('input[name="promo"]');
        $ATUserPromo = $ATform.find('#edit-promokod');
        $ATtransfer2Ukraine = $ATform.find('#edit-transfer-to-ukraine');
        $ATuserFields = $ATform.find('#edit-price, #edit-volume, #edit-age, #edit-city, #edit-euro', '#edit-batareya-kvt-ch');
        $ATauctionValidateInfo = $('.calc-auction__error');

        // 2. Start of logic.
        $ATsubmitButton.hide();
        getDataFromTables();

        $ATform.find('.form-item-fuel input[type=radio]').on('change', function() {
          makeCalc();
        });

        $ATsubmitButton.on('click', function(e) {
          e.preventDefault();
          makeCalc();
        });

        $ATuserFields.on('change', function() {
          //makeCalc();
        });

        // Update Port Select Element
        $ATauction.on('change', function() {
          checkPromoCode();
          AT.auctionName = $(this).val();
          updatePortOptions(AT.auctionName, AT.promo, false);

          if (AT.auctionName === 'Copart') {
            calcCopartFees();
            calcCopartBidFees();
            const copartTotal = AT.copartPrice + AT.copartBidFees + AT.copartOtherFees;
            $ATauctionValue.val('$' + copartTotal);
            $ATsubmitButton.show();
          }

          if (AT.auctionName === 'IAAI') {
            calcIAAIFees();
            calcIAAIBidFees();
            const iaaiTotal = AT.iaaiPrice + AT.iaaiBidFees + AT.iaaiOtherFees;
            $ATauctionValue.val('$' + iaaiTotal);
            $ATsubmitButton.show();
          }

        });

        // Update Land Transfer Price
        $ATport.on('change', function() {
          const selectedData = $(this).find('option:selected').data();
          AT.landTargetCode = selectedData.Target;

          findLandTransfer(selectedData, AT.landTargetCode);
        });

        $ATcustomsInfoTrigger.on('click', function(){
          $ATcustomsInfoTrigger.toggleClass('opened');
        });

        $('#calc-auction-btn').click(function(event) {
          event.preventDefault(); // Prevent the form from submitting normally
          $loadingScreen.show();

          let url = $('#calc-auction-input').val(); // Get the URL from the input field
          const auctionApi = 'https://american-trade-cargoloop-078d8c01b4ed.herokuapp.com/';

          let apiAuction;

          if (url.includes('copart.com')) {
            url = auctionApi + 'get-copart?q=' + url;
            $ATauction.val('Copart');
            $ATauction.trigger('change');
            $ATauctionValidateInfo.hide();
            apiAuction = 'copart';
          } else if (url.includes('iaai.com')) {
            url = auctionApi + 'get-iaai?q=' + url;
            $ATauction.val('IAAI');
            $ATauction.trigger('change');
            $ATauctionValidateInfo.hide();
            apiAuction = 'iaai';
          }
          else {
            $ATauctionValidateInfo.show();
            clearAuctionValues();
            $loadingScreen.hide();
          }

          // Send an AJAX request

          if (apiAuction === 'copart' || apiAuction === 'iaai') {
            $.ajax({
              type: 'GET',
              url: url,
              success: function(response) {
                console.log('Response:', response);
                processAuctionValues(response, apiAuction);
                $loadingScreen.hide();
              },
              error: function(xhr, status, error) {
                console.error('Error:', error);
                $loadingScreen.hide();
              }
            });
          }

        });

      });
    }
  };
}(jQuery, Drupal));
