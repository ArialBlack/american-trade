function clearAuctionValues() {
  jQuery('.auctions__title').text('');
  jQuery('.auctions__lot').text('');
  jQuery('.auctions__imgURL').attr('src', '');
  jQuery('.auctions__vinCode').text('');
  jQuery('.auctions__titleCode').text('');
  jQuery('.auctions__odometer').text('');
  jQuery('.auctions__currentBid').text('');
  jQuery('.auctions__buyNow').text('');
  jQuery('.auctions__keys').text('');
  jQuery('.auctions__engine').text('');
  jQuery('.auctions__fuel').text('');
  jQuery('.auctions__loss').text('');
  jQuery('.auctions__primaryDamage').text('');
  jQuery('.auctions__secondaryDamage').text('');
  jQuery('.auctions__location').text('');
  jQuery('#lot-target').hide();
}

function getAuctionsTitle(title) {
  const yearRegex = /\b\d{4}\b/;
  const match = title.match(yearRegex);

  if (match) {
    jQuery('#edit-age').val(match[0]);
    return match[0];
  } else {
    return null;
  }
}

function getAuctionsPrice(currentBid, buyNow) {
  const priceMax = Math.max(currentBid, buyNow);

  if (priceMax > 1000) {
    jQuery('#edit-price').val(priceMax);
    return priceMax;
  }
  else {
    return 0;
  }
}

function getEngineVolume(engine) {
  if (!engine) {
    return null;
  }

  const volumeRegex = /(\d+(\.\d+)?)L/i;
  const match = engine.match(volumeRegex);

  if (match) {
    jQuery('#edit-volume').val(match[1]);
    return match[1];
  }
  else {
    return null;
  }
}

function getFuelType(fuel) {
  if (!fuel) {
    return;
  }

  switch (fuel) {
    case 'DIESEL':
    case 'Diesel':
      jQuery('input[name="fuel"]').attr('checked', '');
      jQuery('input[name="fuel"][value="Дизель"]').attr('checked', 'checked');
      break;

    case 'HYBRID ENGINE':
    case 'Other':
      jQuery('input[name="fuel"]').attr('checked', '');
      jQuery('input[name="fuel"][value="Гибрид"]').attr('checked', 'checked');
      break;

    case 'ELECTRIC':
    case 'Electric':
      jQuery('input[name="fuel"]').attr('checked', '');
      jQuery('input[name="fuel"][value="Электро"]').attr('checked', 'checked');
      break;

    default:
      jQuery('input[name="fuel"]').attr('checked', '');
      jQuery('input[name="fuel"][value="Бензин"]').attr('checked', 'checked');
      break;
  }
}

function processAuctionValues(response, auction) {
  console.log(response.title);
  if (response.vinCode.length < 10) {
    clearAuctionValues();
    return;
  }

  /*
  {
    "title":"2021 KIA FORTE GT LINE",
    "lot": '64400433',
    "imgURL":"https://cs.copart.com/v1/AUTH_svc.pdoc00001/lpp/0423/0aafe774a2a74fadb47a2830d5f8881e_hrs.jpg",
    "vinCode":"3KPF34AD9ME******",
    "titleCode":"TX - CERTIFICATE OF TITLE",
    "odometer":"58,139 mi (ACTUAL)",
    "currentBid":0,
    "buyNow":10300,
    "keys":"YES",
    "engine":"2.0L  4",
    "fuel":"GAS",
    "primaryDamage":"HAIL",
    "secondaryDamage":"",
    "location":"TX - LONGVIEW" - copart
    "location":"Jacksonville (FL)" - aiia
  }
  */

  jQuery('.auctions__title').text(response.title);
  getAuctionsTitle(response.title);

  jQuery('.auctions__lot').text(response.lot);

  jQuery('.auctions__imgURL').attr('src', response.imgURL);
  jQuery('.auctions__vinCode').text(response.vinCode);
  jQuery('.auctions__titleCode').text(response.titleCode);
  jQuery('.auctions__odometer').text(response.odometer);

  jQuery('.auctions__currentBid').text('$'+ response.currentBid);
  jQuery('.auctions__buyNow').text('$'+ response.buyNow);
  getAuctionsPrice(response.currentBid, response.buyNow);

  jQuery('.auctions__keys').text(response.keys);

  jQuery('.auctions__engine').text(response.engine);
  getEngineVolume(response.engine);

  jQuery('.auctions__fuel').text(response.fuel);
  getFuelType(response.fuel);

  //$('.auctions__loss').text('');
  jQuery('.auctions__primaryDamage').text(response.primaryDamage);
  jQuery('.auctions__secondaryDamage').text(response.secondaryDamage);

  jQuery('.auctions__location').text(response.location);
  let portIsOK = false;

  if (auction === 'copart') {
    if (jQuery('#edit-city option[value="' + response.location + '"]').length) {
      portIsOK = true;
      jQuery('#edit-city').val(response.location).trigger('change');
    }
  }

  if (auction === 'iaai') {
    let port = response.locationStateCode + ' - ' + response.locationStateName;

    if (jQuery('#edit-city option[value="' + port + '"]').length) {
      portIsOK = true;
      jQuery('#edit-city').val(port).trigger('change');
    }
  }

  jQuery('#lot-target').show();

  if (portIsOK) {
    makeCalc();
  }
  else {
    alert('Не знайдено відповідний порт для поточного лота. ' +
      'Будь-ласка, оберіть порт в полі "Доставка авто по США з місця розташування в найближчий порт відправки", ' +
      'та натисніть синю кнопку "Розрахувати", що знаходиться внизу форми.')
  }

}
